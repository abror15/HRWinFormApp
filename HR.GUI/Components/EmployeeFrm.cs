﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using HR.DAL;
using HR.DAL.Models;

namespace HR.GUI.Components
{
    public partial class EmployeeFrm : Form
    {
        private const int DefaultShowRows = 10;
        private const string TemplateFileName = @"D:\docs\myDoc.docx";
        private readonly DataTable _dataTable = new DataTable();
        private int _activePage = 1;
        private List<Employee> _baseEmployees;
        private Employee _emp = new Employee();
        private int _pagesCounter = 1;
        private List<Employee> _tempEmployees;
        private readonly HRDbContext _db = new HRDbContext();

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            _db.Dispose();
        }

        public EmployeeFrm()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            dgvPaging.DataSource = _db.Employees.ToList();
        }

        public void SaveData()
        {

            for (var i = 1500; i < 5000; i++)
            {
                _db.Employees.Add(new Employee
                {
                    FirstName = "Simon" + i,
                    LastName = "Mccain" + i,
                    MiddleName = "Tompson" + i,
                    Address = 25 + i + "Main st London",
                    BirthDate = DateTime.Parse("1955/05/05").AddMonths(i)
                });
                _db.SaveChanges();
            }
        }

        private void RebindGridForPageChange()
        {
            var datasourcestartIndex = (_activePage - 1) * DefaultShowRows;

            _tempEmployees = new List<Employee>();
            for (var i = datasourcestartIndex; i < datasourcestartIndex + DefaultShowRows; i++)
            {
                if (i >= _baseEmployees.Count)
                    break;

                _tempEmployees.Add(_baseEmployees[i]);
            }
            dgvPaging.DataSource = _tempEmployees;
            DgvWidth();
        }

        private void DgvWidth()
        {
            dgvPaging.Columns[0].Width = 100;
            dgvPaging.Columns[1].Width = 100;
            dgvPaging.Columns[2].Width = 100;
            dgvPaging.Columns[3].Width = 100;
            dgvPaging.Columns[4].Width = 100;
            dgvPaging.Columns[5].Width = 100;

            dgvPaging.Columns[0].HeaderText = "ID";
            dgvPaging.Columns[1].HeaderText = "First Name";
            dgvPaging.Columns[2].HeaderText = "Last Name";
            dgvPaging.Columns[3].HeaderText = "Middle Name";
            dgvPaging.Columns[4].HeaderText = "Address";
            dgvPaging.Columns[5].HeaderText = "BirthDate";
        }

        private void RefreshPagination()
        {
            var items = new[] { toolStripButton1, toolStripButton2, toolStripButton3, toolStripButton4, toolStripButton5 };
            var pageStartIndex = 1;

            if (_pagesCounter > 5 && _activePage > 2)
                pageStartIndex = _activePage - 2;

            if (_pagesCounter > 5 && _activePage > _pagesCounter - 2)
                pageStartIndex = _pagesCounter - 4;

            for (var i = pageStartIndex; i < pageStartIndex + 5; i++)
            {
                if (i > _pagesCounter)
                {
                    items[i - pageStartIndex].Visible = false;
                }

                else
                {
                    items[i - pageStartIndex].Visible = true;
                    items[i - pageStartIndex].Text = i.ToString(CultureInfo.InvariantCulture);

                    if (i == _activePage)
                    {
                        items[i - pageStartIndex].BackColor = ColorTranslator.FromHtml("#83D6F6");
                        items[i - pageStartIndex].ForeColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        items[i - pageStartIndex].BackColor = System.Drawing.Color.White;
                        items[i - pageStartIndex].ForeColor = ColorTranslator.FromHtml("#83D6F6");
                    }
                }
            }

            if (_activePage == 1)
                btnBackward.Enabled = btnFirst.Enabled = false;
            else
                btnBackward.Enabled = btnFirst.Enabled = true;

            if (_activePage == _pagesCounter)
                btnForward.Enabled = btnLast.Enabled = false;
            else
                btnForward.Enabled = btnLast.Enabled = true;
        }

        private List<Employee> FillDataforGrid()
        {
            var list = new List<Employee>();

            for (var i = 0; i < _dataTable.Rows.Count; i++)
            {
                var obj = new Employee(int.Parse(_dataTable.Rows[i][0].ToString()), _dataTable.Rows[i][1].ToString(),
                    _dataTable.Rows[i][2].ToString(), _dataTable.Rows[i][3].ToString(),
                    _dataTable.Rows[i][4].ToString(), DateTime.Parse(_dataTable.Rows[i][5].ToString()));

                list.Add(obj);
            }

            return list;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ResetWhenFormIsEmpty();
            PopulateDataGridView();
        }

        private void PopulateDataGridView()
        {

            _dataTable.Columns.Add("ID", typeof(int));
            _dataTable.Columns.Add("First Name", typeof(string));
            _dataTable.Columns.Add("Last Name", typeof(string));
            _dataTable.Columns.Add("Middle Name", typeof(string));
            _dataTable.Columns.Add("Address", typeof(string));
            _dataTable.Columns.Add("BirthDate", typeof(DateTime));

            foreach (var em in _db.Employees)
                _dataTable.Rows.Add(em.Id, em.FirstName, em.LastName, em.MiddleName, em.Address, em.BirthDate);


            _baseEmployees = FillDataforGrid();
            _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
            RefreshPagination();
            RebindGridForPageChange();
        }

        private void PopulateDataGridViewAfterUpdate()
        {

            _dataTable.Reset();
            _dataTable.Columns.Add("ID", typeof(int));
            _dataTable.Columns.Add("First Name", typeof(string));
            _dataTable.Columns.Add("Last Name", typeof(string));
            _dataTable.Columns.Add("Middle Name", typeof(string));
            _dataTable.Columns.Add("Address", typeof(string));
            _dataTable.Columns.Add("BirthDate", typeof(DateTime));

            foreach (var em in _db.Employees)
                _dataTable.Rows.Add(em.Id, em.FirstName, em.LastName, em.MiddleName, em.Address, em.BirthDate);

            _baseEmployees = FillDataforGrid();
            _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
            RefreshPagination();
            RebindGridForPageChange();
        }

        private void dgvPaging_DoubleClick(object sender, EventArgs e)
        {
            if (dgvPaging.CurrentRow != null && dgvPaging.CurrentRow.Index != -1)
            {
                var id = Convert.ToInt32(dgvPaging.CurrentRow.Cells["ID"].Value);

                _emp = _db.Employees.Single(p => p.Id == id);
                txtFirstName.Text = _emp.FirstName;
                txtLastName.Text = _emp.LastName;
                txtMiddleName.Text = _emp.MiddleName;
                txtAddress.Text = _emp.Address;
                dtBirthDate.Value = _emp.BirthDate;

                btnSave.Text = "Update";
                btnDelete.Enabled = true;
                btnWordPrint.Enabled = true;
            }
        }

        private void ResetWhenFormIsEmpty()
        {
            _emp.Id = 0;
            btnSave.Text = "Save";
            btnDelete.Enabled = false;
            btnWordPrint.Enabled = false;
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMiddleName.Text = "";
            txtAddress.Text = "";
            dtBirthDate.Value = DateTime.Now;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtLastName.Text == "")
            {
                MessageBox.Show("Please provide your first name infomation");
            }
            else if (txtLastName.Text == "")
            {
                MessageBox.Show("Please provide your last name infomation");
            }
            else if (txtMiddleName.Text == "")
            {
                MessageBox.Show("Please provide your middle name infomation");
            }
            else if (txtAddress.Text == "")
            {
                MessageBox.Show("Please provide your address infomation");
            }
            else
            {
                _emp.FirstName = txtFirstName.Text;
                _emp.LastName = txtLastName.Text;
                _emp.MiddleName = txtMiddleName.Text;
                _emp.Address = txtAddress.Text;
                _emp.BirthDate = DateTime.Parse(dtBirthDate.Value.ToShortDateString());

                if (_emp.Id == 0)
                    _db.Employees.Add(_emp);
                else
                    _db.Entry(_emp).State = EntityState.Modified;
                _db.SaveChanges();
                PopulateDataGridViewAfterUpdate();
                MessageBox.Show("Successfully updated");
                ResetWhenFormIsEmpty();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ResetWhenFormIsEmpty();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this employee?", "Employee Data",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {

                var state = _db.Entry(_emp);
                if (state.State == EntityState.Detached) _db.Employees.Attach(_emp);
                _db.Employees.Remove(_emp);
                _db.SaveChanges();

                MessageBox.Show("Successfully deleted");
                ResetWhenFormIsEmpty();
                PopulateDataGridViewAfterUpdate();
            }
        }

        private List<Employee> FillDataforGridForOrdering(string order)
        {
            var list = new List<Employee>();

            for (var i = 0; i < _dataTable.Rows.Count; i++)
            {
                var obj = new Employee(int.Parse(_dataTable.Rows[i][0].ToString()), _dataTable.Rows[i][1].ToString(),
                    _dataTable.Rows[i][2].ToString(), _dataTable.Rows[i][3].ToString(),
                    _dataTable.Rows[i][4].ToString(), DateTime.Parse(_dataTable.Rows[i][5].ToString()));

                list.Add(obj);
            }

            switch (order)
            {
                case "name_ac":
                    return list.OrderBy(p => p.FirstName).ToList();
                case "name_dc":
                    return list.OrderByDescending(p => p.FirstName).ToList();
                case "birthDate_ac":
                    return list.OrderBy(p => p.BirthDate).ToList();
            }

            return list.OrderByDescending(p => p.BirthDate).ToList();
        }

        private void btnSortByName_Click(object sender, EventArgs e)
        {
            if (btnSortByName.Text == "Sort Name AC")
            {
                btnSortByName.Text = "Sort Name DC";

                _baseEmployees = FillDataforGridForOrdering("name_ac");
                _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
                RefreshPagination();
                RebindGridForPageChange();
            }
            else
            {
                btnSortByName.Text = "Sort Name AC";

                _baseEmployees = FillDataforGridForOrdering("name_dc");
                _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
                RefreshPagination();
                RebindGridForPageChange();
            }
        }

        private void btnBirthDate_Click(object sender, EventArgs e)
        {

            if (btnBirthDate.Text == "Sort DOB AC")
            {
                btnBirthDate.Text = "Sort DOB DC";

                _baseEmployees = FillDataforGridForOrdering("birthDate_ac");
                _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
                RefreshPagination();
                RebindGridForPageChange();
            }
            else
            {
                btnBirthDate.Text = "Sort DOB AC";

                _baseEmployees = FillDataforGridForOrdering("");
                _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
                RefreshPagination();
                RebindGridForPageChange();
            }
        }

        private void txtSearchValue_TextChanged(object sender, EventArgs e)
        {
            var searchstring = txtSearchValue.Text.ToLower();
            if (!string.IsNullOrWhiteSpace(searchstring))
                PopulateDataGridViewForSearch(searchstring);
            else
                PopulateDataGridViewAfterUpdate();
        }

        private void PopulateDataGridViewForSearch(string searchString)
        {

            _dataTable.Reset();
            _dataTable.Columns.Add("ID", typeof(int));
            _dataTable.Columns.Add("First Name", typeof(string));
            _dataTable.Columns.Add("Last Name", typeof(string));
            _dataTable.Columns.Add("Middle Name", typeof(string));
            _dataTable.Columns.Add("Address", typeof(string));
            _dataTable.Columns.Add("BirthDate", typeof(DateTime));
            foreach (var em in _db.Employees.Where(p =>
                p.FirstName.ToLower().Contains(searchString) || p.LastName.ToLower().Contains(searchString) ||
                p.MiddleName.ToLower().Contains(searchString)))
                _dataTable.Rows.Add(em.Id, em.FirstName, em.LastName, em.MiddleName, em.Address, em.BirthDate);

            _baseEmployees = FillDataforGrid();
            _pagesCounter = Convert.ToInt32(Math.Ceiling(_baseEmployees.Count * 1.0 / DefaultShowRows));
            RefreshPagination();
            RebindGridForPageChange();
        }

        private void btnWordPrint_Click(object sender, EventArgs e)
        {
            SearchAndReplaceWord(TemplateFileName);
        }

        private void SearchAndReplaceWord(string document)
        {
            using (var wordDoc = WordprocessingDocument.Open(document, true))
            {
                var docText = "";
                using (var rd = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = rd.ReadToEnd();
                }

                var regexText = new Regex("fullName");
                var regexText1 = new Regex("dateOfBirth");
                var regexText2 = new Regex("address");
                docText = regexText.Replace(docText, _emp.FirstName + " " + _emp.LastName + " " + _emp.MiddleName);
                docText = regexText1.Replace(docText, _emp.BirthDate.ToShortDateString());
                docText = regexText2.Replace(docText, _emp.Address);
                using (var sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }
        }

        private void btnPrintExcel_Click(object sender, EventArgs e)
        {
            CreateExcelDoc(@"D:\employees.xlsx");
        }

        private void CreateExcelDoc(string fileName)
        {
            using (var document = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook))
            {

                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet();

                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                var sheet = new Sheet
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "Employees"
                };
                sheets.Append(sheet);
                workbookPart.Workbook.Save();
                var employees = new List<Employee>();
                foreach (DataGridViewRow myRow in dgvPaging.Rows)
                {
                    var em = new Employee
                    {
                        Id = int.Parse(myRow.Cells["ID"].Value.ToString()),
                        FirstName = myRow.Cells["FirstName"].Value.ToString(),
                        LastName = myRow.Cells["LastName"].Value.ToString(),
                        MiddleName = myRow.Cells["MiddleName"].Value.ToString(),
                        Address = myRow.Cells["Address"].Value.ToString(),
                        BirthDate = DateTime.Parse(myRow.Cells["BirthDate"].Value.ToString())
                    };

                    employees.Add(em);
                }

                var sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                //constructing header
                var row = new Row();
                row.Append(
                    ConstructCell("ID", CellValues.String),
                    ConstructCell("Full Name", CellValues.String),
                    ConstructCell("Address", CellValues.String),
                    ConstructCell("Date of birth", CellValues.String)
                );
                //insert header data into the sheet data
                sheetData.AppendChild(row);
                //inserting each employees
                foreach (var employee in employees)
                {
                    row = new Row();
                    row.Append(
                        ConstructCell(employee.Id.ToString(), CellValues.Number),
                        ConstructCell(employee.FirstName + " " + employee.LastName + " " + employee.MiddleName,
                            CellValues.String),
                        ConstructCell(employee.Address, CellValues.String),
                        ConstructCell(employee.BirthDate.ToString("yyyy/MM/dd"), CellValues.String)
                    );
                    sheetData.AppendChild(row);
                }

                worksheetPart.Worksheet.Save();
            }
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void btnBackward_Click(object sender, EventArgs e)
        {

            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {

            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {

            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            var toolStripButton = (ToolStripButton)sender;

            if (toolStripButton == btnBackward)
                _activePage--;
            else if (toolStripButton == btnForward)
                _activePage++;
            else if (toolStripButton == btnLast)
                _activePage = _pagesCounter;
            else if (toolStripButton == btnFirst)
                _activePage = 1;
            else
                _activePage = Convert.ToInt32(toolStripButton.Text, CultureInfo.InvariantCulture);

            if (_activePage < 1)
                _activePage = 1;
            else if (_activePage > _pagesCounter)
                _activePage = _pagesCounter;

            RebindGridForPageChange();
            RefreshPagination();
        }

    }
}