﻿using System;
using System.Windows.Forms;
using HR.GUI.Components;

namespace HR.GUI
{
    public partial class MianFrm : Form
    {
        public MianFrm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new EmployeeFrm().Show();
        }
    }
}
