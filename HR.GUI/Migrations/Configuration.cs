using System.Data.Entity.Migrations;

namespace HR.GUI.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<HR.DAL.HRDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "HR.DAL.HRDbContext";
        }

        protected override void Seed(HR.DAL.HRDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
