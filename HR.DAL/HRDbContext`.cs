﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.DAL
{
    public partial class HRDbContext
    {
        public HRDbContext() : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
        }
    }
}
