﻿using System.Data.Entity;
using HR.DAL.Models;

namespace HR.DAL
{
    public partial class HRDbContext : DbContext
    {
      
        public DbSet<Employee> Employees { get; set; }
    }
}