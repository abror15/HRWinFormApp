﻿using System;

namespace HR.DAL.Models
{
    public class Employee
    {
        public Employee()
        {
        }

        public Employee(int id, string firstName, string lastName, string middleName, string address, DateTime birthDate)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            Address = address;
            BirthDate = birthDate;
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Address { get; set; }

        public DateTime BirthDate { get; set; }

    }
}